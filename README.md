# TK Commander

###### A Flutter project for Android, iOS, Windows and Linux. That Dynamically communicates with Micro-Controllers over UDP/TCP.

I love LEGO and when I was a kid I used the LEGO Mindstorms set to do all sorts of fun things. 
Now 20 years later I wanted a similar experience for my kids. 
When I saw the price tag I knew I could never afford to have the fun I really wanted to have.
So I created the TK Commander project to allow the remote control of toys that my kids and I make.

## Getting Started
### Build your First device.
I recomend an ESP32. But keep in mind the Harware requirment is a network enabled computer/micro-controller.

#### Implamnted Devices
* [ESP32 with Micro-Python.](https://gitlab.com/TK-Commander/esp-micropython)

After you set up your device by loading and running the code open this applicaiton.
If you are connected to the same network as your device you should see the device show up in the list.

## Build Status 
[![Codemagic build status](https://api.codemagic.io/apps/62a9e31b738047693eb650e0/62a9e31b738047693eb650df/status_badge.svg)](https://codemagic.io/apps/62a9e31b738047693eb650e0/62a9e31b738047693eb650df/latest_build)
