import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:tkcommanderui/data/micro_device_api.dart';
import 'package:tkcommanderui/view/widget/micro_device_card.dart';

class MicroDeviceList extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  MicroDeviceList({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    MicroDeviceApi mda = MicroDeviceApi();
    return Scaffold(
      // https://www.youtube.com/watch?v=YY-_yrZdjGc
      body: CustomScrollView(slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          stretch: true,
          expandedHeight: 200.0,
          actions: [
            IconButton(
              icon: const Icon(
                Icons.refresh_rounded,
              ),
              color: Theme.of(context).colorScheme.secondary,
              tooltip: 'Request all devices to send the "ident" message',
              onPressed: () => mda.requestRefresh(),
            ),
          ],
          flexibleSpace: FlexibleSpaceBar(
            stretchModes: const [
              StretchMode.blurBackground,
              StretchMode.zoomBackground,
            ],
            title: Text(
              "TK Commander",
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.bold,
                  fontSize: 25),
            ),
            centerTitle: true,
            background: Image.asset(
              'static/default7.jpg',
              fit: BoxFit.cover,
            ),
          ),
        ),
        Obx(() {
          if (mda.remoteList.isNotEmpty) {
            return SliverList(
              delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) => MicorDeviceCard(
                      name: mda.remoteList.keys.elementAt(index)),
                  childCount: mda.remoteList.keys.length),
            );
          } else {
            return const SliverToBoxAdapter(
              child: SizedBox(
                height: 400,
                child: Center(
                  child: CircularProgressIndicator(strokeWidth: 10),
                ),
              ),
            );
          }
        })
      ]),
    );
  }
}
