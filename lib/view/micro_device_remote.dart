import 'package:flutter/material.dart';
import 'package:get/get.dart';
// ignore: depend_on_referenced_packages
import 'package:vector_math/vector_math_64.dart' as vector;

import 'package:tkcommanderui/data/micro_device_api.dart';
import 'package:tkcommanderui/view/widget/remote_view.dart';

class MicroDeviceRemote extends StatelessWidget {
  final String name;
  MicroDeviceRemote({Key? key, required this.name}) : super(key: key);
  final MicroDeviceApi mda = Get.find<MicroDeviceApi>();

  @override
  Widget build(BuildContext context) {
    double viewerSize = 1000;
    // double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    // TransformationController tc = TransformationController(
    //   Matrix4.translation(
    //     vector.Vector3(
    //       (-viewerSize + screenWidth) / 2,
    //       (-viewerSize + screenHeight) / 2,
    //       0,
    //     ),
    //   ),
    // );
    mda.loadRemote(name);
    return SafeArea(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () => Get.back(),
          tooltip: 'Go to list',
          child: const Icon(Icons.home_filled),
        ),
        body: InteractiveViewer.builder(
          boundaryMargin: const EdgeInsets.all(40.0),
          minScale: 0.001,
          maxScale: 50,
          // transformationController: tc,
          builder: (BuildContext context, vector.Quad quad) {
            return Center(
              child: SizedBox(
                width: viewerSize,
                height: viewerSize,
                child: Container(
                  padding: const EdgeInsets.all(32.0),
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                        Colors.orange,
                        Colors.red,
                        Colors.yellowAccent
                      ],
                    ),
                  ),
                  child: Obx(() => RemoteView(
                        ip: mda.remoteList[name]['ip'],
                        remoteActions: mda.activeRemoteActions,
                        displaySize: viewerSize,
                      )),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
