import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:tkcommanderui/data/local_storage_api.dart';
import 'package:tkcommanderui/data/micro_device_api.dart';

import 'package:tkcommanderui/view/micro_device_remote.dart';
import 'package:tkcommanderui/view/micro_device_config.dart';

class MicorDeviceCard extends StatelessWidget {
  final String name;
  MicorDeviceCard({Key? key, required this.name}) : super(key: key);

  final cm = LocalStorage();
  final MicroDeviceApi mda = Get.find<MicroDeviceApi>();

  @override
  Widget build(BuildContext context) {
    dynamic mdr = mda.remoteList[name];
    return Container(
      height: 100,
      constraints: const BoxConstraints(
        maxWidth: 400,
      ),
      decoration: BoxDecoration(
        color: mdr['active'] == true
            ? Theme.of(context).backgroundColor
            : Theme.of(context).shadowColor,
        shape: BoxShape.rectangle,
      ),
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: Card(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          color: mdr['active'] == true
              ? const Color(0xFF424242)
              : Theme.of(context).shadowColor,
          elevation: 5,
          child: Stack(
            alignment: Alignment.center,
            children: [
              AbsorbPointer(
                absorbing: mdr['active'] == false,
                child: GestureDetector(
                  onTap: () => Get.to(() => MicroDeviceRemote(name: name)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          height: 100,
                          width: 100,
                          clipBehavior: Clip.antiAlias,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset(cm.getMicroDeviceImg(name)),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Text(
                            "$name\nIP: ${mdr['ip']}",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Theme.of(context).colorScheme.secondary,
                                fontWeight: FontWeight.bold,
                                fontSize: 25),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => Get.to(() => MicroDeviceConfig(name: name)),
                child: Align(
                  alignment: const Alignment(1, -1),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Icon(
                      Icons.settings,
                      color: Theme.of(context).colorScheme.secondary,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
