import 'package:flutter/material.dart';

import 'package:tkcommanderui/data/udb_com.dart';

class RemoteButton extends StatelessWidget {
  final String ip;
  final String name;
  final String cmd;
  const RemoteButton(
      {Key? key, required this.ip, required this.name, required this.cmd})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          sendMessageUDP(ip, "$cmd|1");
        },
        child: Text(name),
      ),
    );
  }
}
