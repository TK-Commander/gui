import 'package:flutter/material.dart';
import 'flutter_joystick/flutter_joystick.dart';

import 'package:tkcommanderui/data/udb_com.dart';
import 'package:tkcommanderui/data/local_storage_api.dart';

class RemoteJoystick extends StatefulWidget {
  final String ip;
  final String name;
  const RemoteJoystick({Key? key, required this.ip, required this.name})
      : super(key: key);

  @override
  State<RemoteJoystick> createState() => _JoystickState();
}

class _JoystickState extends State<RemoteJoystick> {
  double _x = 100;
  double _y = 100;
  final JoystickMode _joystickMode = JoystickMode.all;

  LocalStorage lStore = LocalStorage();

  @override
  Widget build(BuildContext context) {
    String cmdX = lStore.getJoyX(widget.name);
    String cmdY = lStore.getJoyY(widget.name);
    return SafeArea(
      child: Stack(
        children: [
          Container(
            color: Colors.green,
          ),
          Align(
            alignment: const Alignment(0, 0.8),
            child: Joystick(
              mode: _joystickMode,
              listener: (details) {
                setState(() {
                  _x = _x + details.x;
                  _y = _y + details.y;
                  sendMessageUDP(widget.ip, "$cmdX|${_x - 100}");
                  sendMessageUDP(widget.ip, "$cmdY|${_y - 100}");
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}

class JoystickModeDropdown extends StatelessWidget {
  final JoystickMode mode;
  final ValueChanged<JoystickMode> onChanged;

  const JoystickModeDropdown(
      {Key? key, required this.mode, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 150,
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: FittedBox(
          child: DropdownButton(
            value: mode,
            onChanged: (v) {
              onChanged(v as JoystickMode);
            },
            items: const [
              DropdownMenuItem(
                  value: JoystickMode.all, child: Text('All Directions')),
              DropdownMenuItem(
                  value: JoystickMode.horizontalAndVertical,
                  child: Text('Vertical And Horizontal')),
              DropdownMenuItem(
                  value: JoystickMode.horizontal, child: Text('Horizontal')),
              DropdownMenuItem(
                  value: JoystickMode.vertical, child: Text('Vertical')),
            ],
          ),
        ),
      ),
    );
  }
}
