import 'package:flutter/material.dart';

import 'package:tkcommanderui/data/local_storage_api.dart';

// import 'package:tkcommanderui/view/widget/remote_joystick.dart';
import 'package:tkcommanderui/view/widget/remote_slider.dart';
import 'package:tkcommanderui/view/widget/remote_switch.dart';
import 'package:tkcommanderui/view/widget/remote_button.dart';

class RemoteView extends StatelessWidget {
  final String ip;
  final Map<String, dynamic> remoteActions;
  final double displaySize;
  RemoteView(
      {Key? key,
      required this.ip,
      required this.remoteActions,
      required this.displaySize})
      : super(key: key);

  final lStore = LocalStorage();

  @override
  Widget build(BuildContext context) {
    List<Widget> controlles = [];
    double square = 200;
    double newLineCount = (displaySize / square) - 1;
    Offset currentPos = Offset.zero;
    double count = 0;
    for (var cmd in remoteActions.keys) {
      // print('$cmd, $count, $newLineCount  .');
      String name = cmd.split('|')[1];
      Widget tmp = const Text('not set');
      if ('1' == remoteActions[cmd]['widget']) {
        tmp = RemoteButton(ip: ip, name: name, cmd: cmd);
      } else if ('2' == remoteActions[cmd]['widget']) {
        tmp = RemoteSwitch(ip: ip, name: name, cmd: cmd);
      } else if ('3' == remoteActions[cmd]['widget']) {
        tmp = RemoteSlider(ip: ip, name: name, cmd: cmd, min: -100);
      } else if ('4' == remoteActions[cmd]['widget']) {
        tmp = RemoteSlider(ip: ip, name: name, cmd: cmd, min: 0);
      } else {
        continue;
      }
      Offset pos = lStore.getPossition(name);
      if (pos == Offset.zero) {
        var dx = currentPos.dx + square;
        var dy = currentPos.dy;
        if ((count % newLineCount) == 1) {
          // print('new line');
          dy = currentPos.dy + square;
          dx = 0;
        }
        currentPos = Offset(dx, dy);
        pos = Offset(dx, dy);
      }
      controlles.add(Positioned(
        top: pos.dy,
        left: pos.dx,
        height: square,
        width: square,
        child: Container(
          color: Colors.red,
          height: square,
          width: square,
          child: Stack(
            children: [
              Text(name),
              tmp,
            ],
          ),
        ),
      ));
      count++;
    }

    return Stack(
      alignment: Alignment.center,
      children: controlles,
    );
  }
}
