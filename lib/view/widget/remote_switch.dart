import 'package:flutter/material.dart';

import 'package:tkcommanderui/data/udb_com.dart';

class RemoteSwitch extends StatefulWidget {
  final String ip;
  final String name;
  final String cmd;
  const RemoteSwitch(
      {Key? key, required this.ip, required this.name, required this.cmd})
      : super(key: key);

  @override
  State<RemoteSwitch> createState() => _RemoteSwitch();
}

class _RemoteSwitch extends State<RemoteSwitch> {
  bool isSwitched = false;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Switch(
        value: isSwitched,
        onChanged: (value) {
          sendMessageUDP(widget.ip, "${widget.cmd}|$value");
          setState(() {
            isSwitched = value;
          });
        },
        activeTrackColor: Colors.lightGreenAccent,
        activeColor: Colors.green,
      ),
    );
  }
}
