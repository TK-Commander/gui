import 'package:flutter/material.dart';

import 'package:tkcommanderui/data/udb_com.dart';

class RemoteSlider extends StatefulWidget {
  final String ip;
  final String name;
  final String cmd;
  final double min;
  const RemoteSlider(
      {Key? key,
      required this.ip,
      required this.name,
      required this.cmd,
      required this.min})
      : super(key: key);

  @override
  State<RemoteSlider> createState() => _RemoteSlider();
}

class _RemoteSlider extends State<RemoteSlider> {
  double _currentSliderValue = 0;
  @override
  Widget build(BuildContext context) {
    return Slider(
      value: _currentSliderValue,
      max: 100,
      min: widget.min,
      divisions: 200,
      label: widget.name,
      onChanged: (double value) {
        setState(() {
          _currentSliderValue = value;
        });
        sendMessageUDP(widget.ip, "${widget.cmd}|$value");
      },
    );
  }
}
