import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:xen_popup_card/xen_card.dart';

import 'package:tkcommanderui/data/micro_device_api.dart';
import 'package:tkcommanderui/view/widget/config_switch.dart';

class ConfigView extends StatelessWidget {
  final String host;
  final String sectionKey;
  final Map<String, dynamic> formMap;
  ConfigView({
    Key? key,
    required this.host,
    required this.sectionKey,
    required this.formMap,
  }) : super(key: key);

  final MicroDeviceApi mda = Get.find<MicroDeviceApi>();

  @override
  Widget build(BuildContext context) {
    Widget _formRow(String field, dynamic value,
        [String section = '', int index = -1]) {
      myCb(dynamic val) {
        print("myCb $host, $section, $index, $field, $val");
        if (section.isNotEmpty) {
          if (index < 0) {
            mda.configUpdateSection(host, section, field, val);
          } else {
            mda.configUpdateListSection(host, section, index, field, val);
          }
        } else {
          mda.configUpdateField(host, field, val);
        }
      }

      Widget myInput =
          Text("Unsupported Type: $section, $index, $field, $value");
      if (value is List) {
        myInput = Expanded(
          child: TextFormField(
            initialValue: value.join(', '),
            onChanged: (dynamic val) {
              try {
                val = val.split(',').cast<int>();
              } catch (e) {
                val = val.split(',');
              }
              myCb(val);
            },
          ),
        );
      } else if (value is String) {
        myInput = Expanded(
          child: TextFormField(
            initialValue: value,
            onChanged: (dynamic val) {
              myCb(val);
            },
          ),
        );
      } else if (value is bool) {
        myInput = Expanded(
          child: ConfigSwitch(
            startVal: value,
            cb: myCb,
          ),
        );
      }
      return Row(
        children: [
          SizedBox(
            width: 110,
            child: Text(field),
          ),
          myInput,
        ],
      );
    }

    Widget elementControls(String sectionKey, int index) {
      return Row(
        children: [
          const Expanded(
            child: Text(''),
          ),
          IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () async {
              mda.confDeleteItem(host, sectionKey, index);
            },
          ),
        ],
      );
    }

    Widget sectionHeader(String sectionKey) {
      return Row(
        children: [
          Text(
            sectionKey,
            textAlign: TextAlign.left,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const Expanded(
            child: Text(''),
          ),
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => showDialog(
              context: context,
              builder: (builder) => XenPopupCard(
                // gutter: gutter,
                cardBgColor: Theme.of(context).backgroundColor,
                body: ListView.builder(
                  itemCount: mda.templatesConfig[sectionKey].keys,
                  itemBuilder: (BuildContext context, int index) {
                    Map<String, dynamic> myMap =
                        mda.templatesConfig[sectionKey];
                    return _formRow(
                      myMap.keys.elementAt(index),
                      myMap.values.elementAt(index),
                      sectionKey,
                      -2,
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget mySection = Text("Unsupported Type: $sectionKey, $formMap");
    if (formMap[sectionKey] is String) {
      mySection = _formRow(sectionKey, formMap[sectionKey]);
    } else if (formMap[sectionKey] is Map<String, dynamic>) {
      List<Widget> tmp = [sectionHeader(sectionKey), const Divider()];
      formMap[sectionKey].keys.forEach((field) {
        // print("map field: $sectionKey|$field");
        tmp.add(_formRow(field, formMap[sectionKey][field], sectionKey));
      });
      mySection = Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: tmp,
      );
    } else if (formMap[sectionKey] is List) {
      List<Widget> tmp = [sectionHeader(sectionKey), const Divider()];
      int dex = 0;
      for (var element in formMap[sectionKey]) {
        tmp.add(elementControls(sectionKey, dex));
        for (var field in element.keys) {
          // print("list of map: $sectionKey|$dex|$field");
          tmp.add(_formRow(
              field, formMap[sectionKey][dex][field], sectionKey, dex));
        }
        tmp.add(const Divider());
        dex++;
      }
      mySection = Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: tmp,
      );
    }
    return mySection;
  }
}
