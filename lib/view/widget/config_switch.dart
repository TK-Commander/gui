import 'package:flutter/material.dart';

class ConfigSwitch extends StatefulWidget {
  final bool startVal;
  final Function cb;
  const ConfigSwitch({Key? key, required this.startVal, required this.cb})
      : super(key: key);

  @override
  State<ConfigSwitch> createState() => _ConfigSwitch();
}

/// private State class that goes with MyStatefulWidget
class _ConfigSwitch extends State<ConfigSwitch> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    if (widget.startVal) {
      isSwitched = true;
    }
    return Center(
      child: Switch(
        value: isSwitched,
        onChanged: (value) {
          widget.cb(value);
          setState(() {
            isSwitched = value;
          });
          print("$value = $isSwitched");
        },
      ),
    );
  }
}
