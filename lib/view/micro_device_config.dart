import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:xen_popup_card/xen_card.dart';

import 'package:tkcommanderui/data/local_storage_api.dart';
import 'package:tkcommanderui/data/micro_device_api.dart';
import 'package:tkcommanderui/view/widget/config_view.dart';

class MicroDeviceConfig extends StatelessWidget {
  final String name;
  MicroDeviceConfig({Key? key, required this.name}) : super(key: key);

  final lStore = LocalStorage();
  final MicroDeviceApi mda = Get.find<MicroDeviceApi>();

  @override
  Widget build(BuildContext context) {
    mda.requestConfig(name);

    return Scaffold(
      // https://www.youtube.com/watch?v=YY-_yrZdjGc
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            floating: true,
            snap: true,
            stretch: true,
            onStretchTrigger: () async {
              mda.requestConfig(name);
            },
            expandedHeight: 200.0,
            actions: [
              IconButton(
                icon: const Icon(Icons.refresh),
                onPressed: () async {
                  mda.requestConfig(name);
                },
              ),
              IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () async {
                  deleteThis();
                },
              ),
              IconButton(
                icon: const Icon(Icons.send),
                tooltip:
                    'Send new config to Micro Controller. Micro Controller will Reboot',
                onPressed: () {
                  mda.sendConfig(name);
                  deleteThis();
                },
              ),
            ],
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: const [
                StretchMode.blurBackground,
                StretchMode.zoomBackground,
              ],
              title: Text(
                name,
                style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
              centerTitle: true,
              background: Image.asset(
                'static/default6.jpg',
                fit: BoxFit.cover,
              ),
            ),
          ),
          Obx(() {
            if (mda.activeConfig[name] is Map) {
              return SliverList(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) => ConfigView(
                        host: name,
                        formMap: mda.activeConfig[name],
                        sectionKey:
                            mda.activeConfig[name].keys.elementAt(index)),
                    childCount: mda.activeConfig[name].keys.length),
              );
            } else {
              return const SliverToBoxAdapter(
                child: SizedBox(
                  height: 400,
                  child: Center(
                    child: CircularProgressIndicator(strokeWidth: 10),
                  ),
                ),
              );
            }
          }),
        ],
      ),
    );
  }

  void deleteThis() {
    Get.defaultDialog(
      title: 'Do you want to remove cashed Micro Device?',
      middleText: 'If you changed the Hostname this is a good Idea.',
      actions: [
        IconButton(
          icon: const Icon(Icons.delete),
          color: Colors.red[40],
          tooltip:
              'If you renamed this Micro Controller you may want to delete the old refferace.',
          onPressed: () {
            mda.deleteMicroDevice(name);
            Get.back();
            Get.back();
          },
        ),
      ],
      radius: 10.0,
    );
  }

  // void addElemant(String section) {
  //   Map<String, dynamic> map = mda.templatesConfig[section];
  //   Widget tmp = CustomScrollView(
  //     slivers: [
  //       SliverList(
  //         delegate: SliverChildBuilderDelegate(
  //             (BuildContext context, int index) =>
  //                 _sectionBuilder(map, map.keys.elementAt(index)),
  //             childCount: map.keys.length),
  //       ),
  //       IconButton(
  //         icon: const Icon(Icons.add),
  //         color: Colors.red[40],
  //         tooltip:
  //             'If you renamed this Micro Controller you may want to delete the old refferace.',
  //         onPressed: () {
  //           mda.confAddMap(name, section, map);
  //           Get.back();
  //         },
  //       ),
  //     ],
  //   );
  // Widget cardWithBodyOnly() => TextButton(
  //   onPressed: () => showDialog(
  //     context: context,
  //     builder: (builder) => XenPopupCard(
  //       body: ListView(
  //         children: const [
  //           Text("body"),
  //         ],
  //       ),
  //     ),
  //   ),
  //   child: const Text("open card with body only"),
  // );
  // }
}
