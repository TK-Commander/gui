import 'dart:convert';
import 'dart:async';
import 'dart:io';

class EventListenerUDP {
  late StreamController _streamCon;
  late Stream<Datagram> _streamUDP;

  Stream<Datagram> get myStream => _streamUDP;

  EventListenerUDP() {
    _streamCon = StreamController<Datagram>();
    _streamUDP =
        (_streamCon as StreamController<Datagram>).stream.asBroadcastStream();
  }

  Future<void> start() async {
    await RawDatagramSocket.bind(InternetAddress.anyIPv4, 44445)
        .then((socket) async {
      socket.writeEventsEnabled = false;
      socket.listen((RawSocketEvent e) {
        if (e == RawSocketEvent.read) {
          // print('Socket Read Event');
          _streamCon.add(socket.receive());
        }
      });
    });
  }
}

Future<void> broadcastUDP(String broadcastMessage) async =>
    await RawDatagramSocket.bind(InternetAddress.anyIPv4, 44445)
        .then((socket) async {
      socket.broadcastEnabled = true;
      var ethSubNets = [];
      print('broadcast,  $broadcastMessage');
      for (var interface in await NetworkInterface.list()) {
        for (var addr in interface.addresses) {
          if (('IPv4' == addr.type.name) != (addr.isLoopback)) {
            ethSubNets.add(addr.rawAddress.sublist(0, 3).join('.'));
            print("${addr.rawAddress.sublist(0, 3)}");
          }
        }
      }
      for (var net in ethSubNets) {
        for (int i = 0; i < 255; i++) {
          socket.send(const Utf8Codec().encode(broadcastMessage),
              InternetAddress("$net.$i"), 44444);
        }
      }
    });

Future<void> sendMessageUDP(String ipAddress, String message) async =>
    await RawDatagramSocket.bind(InternetAddress.anyIPv4, 44445).then((socket) {
      print('sendMessageUDP: $ipAddress, $message');
      socket.send(
          const Utf8Codec().encode(message), InternetAddress(ipAddress), 44444);
    });

Future<void> eventListenerTCP(
    Future<void> Function(String adder, String msg) cb) async {
  Future<ServerSocket> serverFuture =
      ServerSocket.bind(InternetAddress.anyIPv4, 44445);
  serverFuture.then((ServerSocket server) {
    server.listen((Socket socket) {
      // print("incomingTCP: ${socket.remoteAddress.address}\n\t$socket");
      socket.listen((data) {
        String result = String.fromCharCodes(data);
        print("tcp event: \t$result");
        cb(socket.remoteAddress.address, result);
      });
    });
  });
}
