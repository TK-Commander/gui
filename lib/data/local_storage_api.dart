import 'dart:convert';
import 'dart:math';

import 'package:flutter/gestures.dart';
import 'package:get_storage/get_storage.dart';

class LocalStorage {
  dynamic data;
  Random? randomNumberGenerator;

  LocalStorage() {
    data = GetStorage();
    randomNumberGenerator = Random();
  }

  String getMicroDevices() {
    if (data.read('devices') != null) {
      return data.read('devices');
    }
    return "";
  }

  void setMicroDevices(String val) {
    data.write('devices', val);
  }

  Map<String, dynamic> getMicroDevice(String key) {
    if (data.read('devices').toString().contains(key)) {
      return jsonDecode(data.read('devices'))[key];
    }
    return <String, dynamic>{};
  }

  String getMicroDeviceConfig(String key) {
    if (data.read('$key:config') != null) {
      return data.read('$key:config');
    }
    return '';
  }

  void setMicroDeviceConfig(String key, String val) {
    data.write('$key:config', val);
  }

  String getMicroDeviceImg(String key) {
    if (data.read('$key:img') != null) {
      return data.read('$key:img');
    }
    return randomNumberGenerator!.nextBool()
        ? 'static/default0.jpg'
        : 'static/default1.jpg';
  }

  void setMicroDeviceImg(String key, int val) {
    data.write('$key:img', val);
  }

  Offset getPossition(String key) {
    if (data.read('$key:Possition') != null) {
      return data.read('$key:Possition');
    }
    return Offset.zero;
  }

  void setPossition(String key, Offset info) {
    data.write('$key:Possition', info);
  }

  String getJoyX(String key) {
    if (data.read('$key:JoyX') != null) {
      return data.read('$key:JoyX');
    }
    return "";
  }

  void setJoyX(String key, String cmd) {
    data.write('$key:JoyX', cmd);
  }

  String getJoyY(String key) {
    if (data.read('$key:JoyY') != null) {
      return data.read('$key:JoyY');
    }
    return "";
  }

  void setJoyY(String key, String cmd) {
    data.write('$key:JoyY', cmd);
  }
}
