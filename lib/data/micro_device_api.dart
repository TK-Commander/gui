// ignore_for_file: non_constant_identifier_names

import 'dart:convert';
import 'dart:async';

import 'package:get/get.dart';

import 'package:tkcommanderui/data/udb_com.dart';
import 'package:tkcommanderui/data/local_storage_api.dart';

class MicroDeviceApi extends GetxController {
  RxMap<String, dynamic> _remoteList = <String, dynamic>{}.obs;
  Map<String, dynamic> get remoteList => _remoteList;

  RxMap<String, dynamic> _activeRemoteActions = <String, dynamic>{}.obs;
  Map<String, dynamic> get activeRemoteActions => _activeRemoteActions;

  final RxMap<String, dynamic> _configData = <String, dynamic>{}.obs;
  Map<String, dynamic> get activeConfig => _configData;

  final String _broadcastMessage = "infoReq";

  final LocalStorage lStore = LocalStorage();

  @override
  void onInit() {
    super.onInit();
    // print("int MicroDeviceApi");
    _remoteList = RxMap(jsonDecode(lStore.getMicroDevices()));
    //lStore.setMicroDevices(jsonEncode(_remoteList));
    eventListenerTCP(eventListenerCallBack);
    Future.delayed(const Duration(seconds: 2), () => requestRefresh());
  }

  Future<void> requestRefresh() async {
    // ignore: avoid_function_literals_in_foreach_calls
    _remoteList.keys.forEach((name) async {
      print(name);
      _remoteList[name] = {
        'ip': _remoteList[name]['ip'],
        'actions': _remoteList[name]['actions'],
        'date': DateTime.now().toString(),
        'active': false
      };
    });
    broadcastUDP(_broadcastMessage);
    update();
  }

  Future<void> requestConfig(microDeviceName) async {
    await sendMessageUDP(_remoteList[microDeviceName]['ip'], "config_get");
  }

  Future<void> sendConfig(microDeviceName) async {
    String jsonConfig = jsonEncode(_configData[microDeviceName]);
    await sendMessageUDP(
        _remoteList[microDeviceName]['ip'], 'config_set|$jsonConfig');
    lStore.setMicroDeviceConfig(microDeviceName, jsonConfig);
  }

  Future<void> eventListenerCallBack(String adder, String message) async {
    print('eventListenerCallBack($adder, $message)');
    Map<String, dynamic> messageMap = jsonDecode(message);
    if ('infoReq' == messageMap['ack']) {
      // _configData[messageMap['hostname']] = {};
      _remoteList[messageMap['hostname']] = {
        'ip': adder,
        'actions': messageMap['res'],
        'date': DateTime.now().toString(),
        'active': true
      };
      lStore.setMicroDevices(jsonEncode(_remoteList));
    } else if ('config_get' == messageMap['ack']) {
      lStore.setMicroDeviceConfig(
          messageMap['hostname'], jsonEncode(messageMap['res']));
      loadConfig(messageMap['hostname']);
    } else {
      remoteCallback(messageMap);
    }
  }

  void loadRemote(String host) {
    // print("int remote");
    for (var action in _remoteList[host]['actions']) {
      String component = action['name'];
      for (var function in action.keys) {
        if ('name' != function) {
          _activeRemoteActions["$component|$function"] = {
            'widget': action[function],
            'load': 0
          };
        }
      }
    }
  }

  void remoteCallback(Map<String, dynamic> message) {
    // print("MDremote CB: $message");
    _activeRemoteActions[message['ack']]['load'] = message['res'];
    // update action
  }

  void remoteClear() {
    _activeRemoteActions = <String, dynamic>{}.obs;
  }

  RxMap<String, dynamic> _templatesConfig = <String, dynamic>{}.obs;
  Map<String, dynamic> get templatesConfig => _templatesConfig;

  RxMap<String, dynamic> _docConfig = <String, dynamic>{}.obs;
  Map<String, dynamic> get docConfig => _docConfig;

  void loadConfig(String host) {
    Map<String, dynamic> tmp = jsonDecode(lStore.getMicroDeviceConfig(host));
    _templatesConfig = RxMap(tmp['templates']);
    tmp.remove('templates');
    _docConfig = RxMap(tmp['device_doc']);
    tmp.remove('device_doc');
    _configData[host] = tmp;
  }

  void confAddMap(String host, String section, Map<String, dynamic> map) {
    _configData[host][section].add(map);
  }

  void configUpdateField(String host, String field, dynamic val) {
    _configData[host][field] = val;
  }

  void configUpdateSection(
      String host, String section, String field, dynamic val) {
    _configData[host][section][field] = val;
  }

  void configUpdateListSection(
      String host, String section, int id, String field, dynamic val) {
    _configData[host][section][id][field] = val;
  }

  void confDeleteItem(String host, String section, int id) {
    if (_configData[section] is List) {
      _configData[host][section].removeAt(id);
    }
  }

  void deleteMicroDevice(String host) {
    _remoteList.remove(host);
    lStore.setMicroDevices(jsonEncode(host));
    _configData.remove(host);
    lStore.setMicroDeviceConfig(host, "");
  }
}
